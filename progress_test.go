package progress_test

import (
	"fmt"
	"io"
	"strings"
	"sync"
	"testing"

	. "github.com/fgrosse/gomega-matchers"
	. "github.com/onsi/gomega"
	"gitlab.com/_fgrosse/progress"
)

func TestEvents(t *testing.T) {
	RegisterExtendedTestingT(t)
	out := new(writer)

	pb := progress.Events{Output: out}
	pb.MustStart()

	task := func(progress chan<- string) {
		progress <- "Starting.."
		progress <- "Done"
		close(progress)
	}

	for i := 1; i <= 3; i++ {
		taskName := fmt.Sprintf("Task %d:", i)
		progress := pb.RegisterTask(func(msg string, out io.Writer) {
			fmt.Fprintln(out, taskName, msg)
		})

		go task(progress)
	}

	Eventually(out.Lines).Should(ContainElement(`Task 1: Starting..`))
	Eventually(out.Lines).Should(ContainElement(`Task 2: Starting..`))
	Eventually(out.Lines).Should(ContainElement(`Task 3: Starting..`))

	Eventually(out.Lines).Should(ContainElement(`Task 1: Done`))
	Eventually(out.Lines).Should(ContainElement(`Task 2: Done`))
	Eventually(out.Lines).Should(ContainElement(`Task 3: Done`))

	pb.WaitAndClose()
}

type writer struct {
	sync.Mutex
	lines []string
}

func (w *writer) Write(b []byte) (n int, err error) {
	w.Lock()
	w.lines = append(w.lines, strings.Split(string(b), "\n")...)
	w.Unlock()

	return len(b), nil
}

func (w *writer) Lines() []string {
	w.Lock()
	defer w.Unlock()
	return w.lines
}
