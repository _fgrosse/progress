package progress_test

import (
	"fmt"
	"io"
	"math/rand"
	"time"

	"gitlab.com/_fgrosse/progress"
)

func Example() {
	events := progress.Events{}

	fmt.Println("This text comes before the output of the concurrent events")
	events.MustStart()

	// task is some function that will later be executed concurrently.
	task := func(progress chan<- string) {
		progress <- "Starting"
		n := rand.Intn(2500)
		progress <- fmt.Sprintf("Sleeping for %d ms", n)
		time.Sleep(time.Millisecond * time.Duration(n))
		progress <- "Finished"
		close(progress) // closing the channel signals that this task is done
	}

	// printTask creates a function that prints a task to a Writer.
	// The messages a progress.UpdateFunc gets directly correspond to the strings
	// that are put into the progress channel.
	printTask := func(taskName string) progress.UpdateFunc {
		return func(msg string, out io.Writer) {
			fmt.Fprintln(out, taskName, msg)
		}
	}

	// Now register and start all task concurrently.
	for i := 0; i < 10; i++ {
		taskName := fmt.Sprintf("Task %d", i)
		progress := events.RegisterTask(printTask(taskName))

		go task(progress)
	}

	// Block until all tasks are done
	events.WaitAndClose()
	fmt.Println("Some text after the whole progress output is done")
}
