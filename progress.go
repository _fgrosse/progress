package progress

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/pkg/errors"
)

// The following constants are the used ANSI codes to manipulate the
// cursor on a shell.
// http://en.wikipedia.org/wiki/ANSI_escape_code#Sequence_elements
var (
	ansiEraseLine = fmt.Sprintf("%c[2K\r", 27)
	ansiMoveUp    = fmt.Sprintf("%c[%dA", 27, 1)
)

// Out is the io.Writer that is used to print output to the console if no other
// Writer has been specified.
var Out io.Writer = os.Stdout

// Events is a progress output for multiple tasks that happen concurrently.
// Each task gets its own progress channel where it can publish any update
// events as simple string messages.
//
// Events will render the current status of all tasks on its `Output`
// io.Writer. If this is nil, the package variable `Out` will be used
// (default os.Stdout).
type Events struct {
	// Output is passed in the update function of all registered tasks.
	// If this is nil the package variable `Out` is used (default os.Stdout).
	Output io.Writer

	// DisableScrollback can be set to false in order to disable the cursor
	// scrollback before the registered events are updated. This can be
	// useful if the application is in a debugging mode where debug output
	// is mixed with the update output.
	// If DisableScrollback is true all updates will appear sequentially on
	// the console output.
	DisableScrollback bool

	running bool
	closed  bool

	mutex sync.Mutex
	tasks []*task

	events  chan event
	closing chan chan error // unbuffered channel to synchronize with pending/processing events

	listeners sync.WaitGroup

	// writtenLines is the number of lines the last update has written to Out.
	writtenLines int
}

// The UpdateFunc of each registered task is called whenever any of the
// registered events has indicated an update. Its argument is the last
// message that was assigned to the event that was registered with this
// function.
type UpdateFunc func(lastMsg string, out io.Writer)

type task struct {
	updateFunc UpdateFunc
	msg        string
}

type event struct {
	item *task
	msg  string
}

// ProgressBufferSize is the size of the internal channel that is used to
// process update events.
var ProgressBufferSize = 10

// RegisterTask adds a new task.
// The returned channel can be used to indicate any status updates for the new task.
// Each value that is put into the update channel will cause this Events to
// update automatically after Events.Start was called.
// When such an update occurs the given UpdateFunc of each registered task will
// be called to produce the according output.
//
// Once the task has been completed the update channel must be closed.
//
// Registering a new task on a closed Events will panic.
func (e *Events) RegisterTask(f UpdateFunc) chan<- string {
	if e.closed {
		panic("can not register a task on closed Events")
	}

	e.init()
	t := &task{updateFunc: f}

	e.mutex.Lock()
	e.tasks = append(e.tasks, t)
	e.mutex.Unlock()

	events := make(chan string)
	e.listeners.Add(1) // will be done once the eventListener detects that `events` was closed
	go e.eventListener(t, events)

	return events
}

func (e *Events) init() {
	e.mutex.Lock()

	if e.events == nil {
		e.events = make(chan event, ProgressBufferSize)
	}

	if e.closing == nil {
		e.closing = make(chan chan error)
	}

	e.mutex.Unlock()
}

// An eventListener is started for each registered task.
// It reads all messages from the tasks update channel until it
// is closed
func (e *Events) eventListener(t *task, c <-chan string) {
	defer e.listeners.Done()
	for msg := range c {
		e.events <- event{t, msg}
	}
}

// MustStart is like Start but will panic if an error occurs.
// It is guaranteed to be save to use if you never call WaitAndClose before this function.
func (e *Events) MustStart() {
	err := e.Start()
	if err != nil {
		panic(err)
	}
}

// Start tells the Events bar to start processing all status updates
// for any registered tasks. It is non blocking and returns immediately.
// Use Events.WaitAndClose if you want to wait for tasks to finish.
//
// It is an error to call Start on a Events that was already closed via WaitAndClose
// or that is already running.
func (e *Events) Start() error {
	e.mutex.Lock()
	switch {
	case e.running:
		return errors.New("progress is already running")
	case e.closed:
		return errors.New("progress has already been closed and can not be reused")
	default:
		e.running = true
	}
	e.mutex.Unlock()

	e.init()

	go e.selectLoop()
	return nil
}

func (e *Events) selectLoop() {
	var err error

	for {
		select {
		case evt := <-e.events:
			evt.item.msg = evt.msg
			err = e.update() // TODO use multierror?
		case errc := <-e.closing:
			close(e.events)
			e.running = false
			e.closed = true
			errc <- err
			return
		}
	}
}

func (e *Events) update() error {
	e.mutex.Lock()
	defer e.mutex.Unlock()

	out := e.Output
	if out == nil {
		out = Out
	}

	if !e.DisableScrollback {
		_, err := fmt.Fprint(out, ansiEraseLine)
		for i := 0; i < e.writtenLines; i++ {
			if err != nil {
				continue
			}
			_, err = fmt.Fprint(out, ansiMoveUp+ansiEraseLine)
		}

		if err != nil {
			return errors.Wrap(err, "error while updating events progress")
		}
	}

	writer := &wrappedWriter{Writer: out}
	for _, i := range e.tasks {
		i.updateFunc(i.msg, writer)
	}

	e.writtenLines = writer.lines
	return nil
}

// a wrappedWriter decorates another io.Writer by recording the number of
// new lines that are written to its `Write` function.
type wrappedWriter struct {
	io.Writer
	lines int
}

// Write implements io.Writer.
func (w *wrappedWriter) Write(p []byte) (int, error) {
	w.lines += bytes.Count(p, []byte{'\n'})
	return w.Writer.Write(p)
}

// WaitAndClose blocks until all registered tasks have finished and the
// according events have been processed completely.
// If this Events has not yet been started WaitAndClose returns an error.
// Every call to RegisterTask after you called this function will result
// in an error.
func (e *Events) WaitAndClose() error {
	if e.events == nil {
		return errors.New("progress.Events was not started yet")
	}

	e.listeners.Wait()
	errc := make(chan error)
	e.closing <- errc
	return <-errc
}
